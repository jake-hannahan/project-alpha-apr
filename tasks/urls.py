from django.urls import path
from .views import task_create, task_list, task_edit


urlpatterns = [
    path("create/", task_create, name="create_task"),
    path("mine/", task_list, name="show_my_tasks"),
    path("mine/<int:id>/edit", task_edit, name="edit_task"),
]
